$(function() {

	//инциализация 
    $(window).on("scroll", function(e) {
        parallax();
        headerPos();
        goUp();
        animation('.head h1, h6', '.head');
    });

    //css
    $('.container_parallax, .parallax_item').height($(window).height());
    $(window).resize(function() {
        $('.container_parallax, .parallax_item').height($(window).height());
    });

    // зкрытие нав меню на малых экранах
    $('.navbar-collapse.collapse').on('click', function(e) {
        if ($('.navbar-collapse.collapse').hasClass('in')) {
            $('.navbar-toggle').trigger('click');
        }
    });

    //аним сылок
    $('.go_link').on('click', function(e) {
        var valAttr = $(this).attr("href");
        var positionLink = $(valAttr).offset().top - 55;
        $('html, body').stop();
        $('html, body').animate({
            scrollTop: positionLink
        }, 600);
        e.preventDefault();
    });

    
    //анимация заголовка
    function animation(selector, selectorItem) {

        var scrolled = $(document).scrollTop(),
            itemTop = $(selectorItem).offset().top - 100,
            itemBottom = itemTop + $(selectorItem).height();


        if (itemTop < scrolled && itemBottom > scrolled) {
            $(selector).addClass('animation');
        }
    };

    //паралах
    function parallax() {
        var scrolled = $(document).scrollTop();
        $('.parallax_item').each(function(index, el) {
            //шаг параллакса
            var posistion = -(scrolled * (((+$(el).attr("data-speed") + 55) / 2) / 55));
            //серце параллакса
            $(el).css({
                '-webkit-transform': 'translate3d(0, ' + posistion + 'px, 0)',
                '-ms-transform': 'translate3d(0, ' + posistion + 'px, 0)',
                'transform': 'translate3d(0, ' + posistion + 'px, 0)'
            });
        });
    };

    //нав бар
    function headerPos() {
        if ($(window).scrollTop() > 1) {
            $('.navbar__transition').addClass('navbar__transition-pgt');
        } else {
            $('.navbar__transition').removeClass('navbar__transition-pgt');
        }
    };

    //тогл кнопки вверх
    function goUp() {
        var positionShow = $('.head').offset().top,
            nowScroll = $(window).scrollTop();
        if (nowScroll > positionShow) {
            $('.go-up').fadeIn(400);
        } else {
            $('.go-up').fadeOut(400);
        }
    };

    $('.form-horizontal').submit(function(e) {
        e.preventDefault();

        //валидация формы

        $('.name__error, .email__error').css({
            display: ''
        });
        var inValidName = false;
        var inValidEmail = false;
        //фильтрация значений
        var strName = /[a-zа-яё\s]{3,20}/i;
        var strEmail = /[-a-z\d_]{3,15}[@][a-z]{2,6}\.[a-z]{2,6}/i;
        //проверка
        if (!$('#name').val().length) {
            $('.name__error').css({
                display: 'block'
            });
            $('.name__error p').text('Введите имя')
            var inValidName = true;
        } else if (!$('#name').val().match(strName)) {
            $('.name__error').css({
                display: 'block'
            });
            $('.name__error p').text('Имя не корректно');
            var inValidName = true;
        };

        if (!$('#email').val().length) {
            $('.email__error').css({
                display: 'block'
            });
            $('.email__error p').text('Введите email')
            var inValidEmail = true;
        } else if (!$('#email').val().match(strEmail)) {
            $('.email__error').css({
                display: 'block'
            });
            $('.email__error p').text('Email не корректный');
            var inValidEmail = true;
        };

        //валидация
        if (inValidName || inValidEmail) {
            return;
        };

        //отправка на сервер данных
        var method = $(this).attr('method');
        var action = $(this).attr('action');
        var data = $(this).serialize();
        console.log(decodeURIComponent(data));
        $.ajax({
            url: action,
            type: method,
            data: data,
            success: function() {
                respond('Сообщение отправлено');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                var errors = 'Статус: ' + textStatus + '<br/>';
                errors += 'Текст ошибки: ' + errorThrown;
                respond(errors);
            }
        });
        $('#name').val('');
        $('#email').val('');
        $('#textarea').val('');
    });
    //успешная отправка формы
    function respond(text) {
        $('.pop-up__contain').fadeIn();
        $('.panel-body p').html(text);
    };

    //over__hidden
    // закрытие панели
    $('.custom__btn, .pop-up__fone').on('click', function() {
        $(this).closest('.pop-up__contain').fadeOut();
    });

    //надстройка скролинга
    $.srSmoothscroll({
        step: 40,
        speed: 400,
        ease: 'linear'
    });

    //прогресс бар
    $('.progras-bar__value').eq(0).css({
        width: '90%'
    });
    $('.progras-bar__value').eq(1).css({
        width: '80%'
    });
    $('.progras-bar__value').eq(2).css({
        width: '70%'
    });
    $('.progras-bar__value').eq(3).css({
        width: '70%'
    });
    $('.progras-bar__value').eq(4).css({
        width: '80%'
    });
    $('.progras-bar__value').eq(5).css({
        width: '65%'
    });
    $('.pers-skils .progras-bar__value').eq(0).css({
        width: '60%'
    });
    $('.pers-skils .progras-bar__value').eq(1).css({
        width: '90%'
    });
    $('.pers-skils .progras-bar__value').eq(2).css({
        width: '80%'
    });
    $('.pers-skils .progras-bar__value').eq(3).css({
        width: '100%'
    });

    //year
    (function() {
        var year = new Date();
        $('.year').text(year.getFullYear());
    })();

});
